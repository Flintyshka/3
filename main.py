from fastapi import FastAPI, HTTPException
from typing import List
from pydantic import BaseModel
import random
import string

app = FastAPI()

#генерация случайной строки
def get_random_name(length):
    letters = string.ascii_lowercase
    result_str = ''.join(random.choice(letters) for i in range(length))
    return result_str

#генерация случайного пола
def get_random_gender():
    genders = ['male', 'female']
    return random.choice(genders)

#генерация случайной базы данных
def generate_db(num_users):
    return [{"id": i+1, "name": get_random_name(5), "gender": get_random_gender()} for i in range(num_users)]

db = generate_db(6)


class User(BaseModel):
    id: int
    name: str
    gender: str

class New_user(BaseModel):
    name: str
    gender: str


#получение всех строк бд
@app.get("/users", response_model=List[User])
def get_user():
    return db

#фильтрация по полу
@app.get("/users/{user_gender}", response_model=List[User])
def get_filtered_users(user_gender: str, limit: int=1):
    return [user for user in db if user.get("gender") == user_gender][:limit]

#добавление данных в бд
@app.post("/users", response_model=List[User])
def add_new_user(user:New_user):
    new_id = len(db) + 1
    new_user = User(id=new_id, name=user.name, gender=user.gender)
    if user.gender not in ['male', 'female']:
        raise HTTPException(status_code=400, detail="Неверно введен пол")
    db.append(new_user.dict())
    return new_user
